$( document ).ready(function() {
    
	// banner
	$('.slidex').slick({
	  dots: false,
	  infinite: true,
	  speed: 500,
	  fade: true,
	  prevArrow: "<div class='prev'>prev</div>",
	  nextArrow: "<div class='next'>next</div>"
	});

	// product
	$('.product-wrap').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  prevArrow: "<div class='prev'>prev</div>",
	  nextArrow: "<div class='next'>next</div>",
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

});